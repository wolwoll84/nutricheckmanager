package com.ksy.nutricheckmanagerksy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NutriCheckManagerKsyApplication {

    public static void main(String[] args) {
        SpringApplication.run(NutriCheckManagerKsyApplication.class, args);
    }

}
