package com.ksy.nutricheckmanagerksy.advice;

import com.ksy.nutricheckmanagerksy.enums.ResultCode;
import com.ksy.nutricheckmanagerksy.exception.*;
import com.ksy.nutricheckmanagerksy.model.CommonResult;
import com.ksy.nutricheckmanagerksy.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CDatePastException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDatePastException e) {
        return ResponseService.getFailResult(ResultCode.DATE_PAST);
    }

    @ExceptionHandler(CExcessEatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExcessEatException e) {
        return ResponseService.getFailResult(ResultCode.EXCESS_EAT);
    }

    @ExceptionHandler(CEatDateFutureException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CEatDateFutureException e) {
        return ResponseService.getFailResult(ResultCode.EAT_DATE_FUTURE);
    }
}

