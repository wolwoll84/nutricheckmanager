package com.ksy.nutricheckmanagerksy.controller;

import com.ksy.nutricheckmanagerksy.model.HistoryItem;
import com.ksy.nutricheckmanagerksy.model.ListResult;
import com.ksy.nutricheckmanagerksy.service.HistoryService;
import com.ksy.nutricheckmanagerksy.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@Api(tags = "복용내역")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final HistoryService historyService;

    @ApiOperation(value = "복용내역 전체 조회")
    @GetMapping("/all")
    public ListResult<HistoryItem> getHistories() {
        return ResponseService.getListResult(historyService.getHistories(), true);
    }
}
