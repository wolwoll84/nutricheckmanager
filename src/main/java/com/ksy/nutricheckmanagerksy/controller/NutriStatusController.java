package com.ksy.nutricheckmanagerksy.controller;

import com.ksy.nutricheckmanagerksy.model.CommonResult;
import com.ksy.nutricheckmanagerksy.model.HistoryRequest;
import com.ksy.nutricheckmanagerksy.model.ListResult;
import com.ksy.nutricheckmanagerksy.model.TodayPillEatItem;
import com.ksy.nutricheckmanagerksy.service.NutriEatService;
import com.ksy.nutricheckmanagerksy.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "오늘의 복용 현황")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/nutri-status")
public class NutriStatusController {
    private final NutriEatService nutriEatService;

    @ApiOperation(value = "복용내역 등록")
    @PostMapping("/eat/{id}")
    public CommonResult doEat(@PathVariable long id, @RequestBody @Valid HistoryRequest request) {
        nutriEatService.doEat(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "오늘의 복용 현황 조회")
    @GetMapping("/today")
    public ListResult<TodayPillEatItem> getTodayStatus() {
        return ResponseService.getListResult(nutriEatService.getTodayStatus(), true);
    }
}
