package com.ksy.nutricheckmanagerksy.controller;

import com.ksy.nutricheckmanagerksy.model.*;
import com.ksy.nutricheckmanagerksy.service.PillService;
import com.ksy.nutricheckmanagerksy.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "영양제 목록")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pill")
public class PillController {
    private final PillService pillService;

    @ApiOperation(value = "영양제 등록")
    @PostMapping("/data")
    public CommonResult setPill(@RequestBody @Valid PillRequest request) {
        pillService.setPill(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "영양제 전체 조회")
    @GetMapping("/all")
    public ListResult<PillItem> getPills() {
        return ResponseService.getListResult(pillService.getPills(), true);
    }

    @ApiOperation(value = "영양제 검색 조회")
    @GetMapping("/all/search")
    public ListResult<PillItem> getPillsByLikeName(@RequestParam("searchName") String searchName) {
        return ResponseService.getListResult(pillService.getPillsByLikeName(searchName), true);
    }

    @ApiOperation(value = "영양제 재구매 목록 조회")
    @GetMapping("/all/repurchase")
    public ListResult<PillRepurchaseItem> getRepurchase() {
        return ResponseService.getListResult(pillService.getRepurchases(), true);
    }

    @ApiOperation(value = "영양제 상세페이지")
    @GetMapping("/detail/{id}")
    public SingleResult<PillResponse> getPillDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(pillService.getPillDetail(id));
    }

    @ApiOperation(value = "영양제 정보 수정")
    @PutMapping("/info/{id}")
    public CommonResult putPillInfo(@PathVariable long id, @RequestBody @Valid PillInfoUpdateRequest request) {
        pillService.putPillInfo(id, request);

        return ResponseService.getSuccessResult();
    }
}
