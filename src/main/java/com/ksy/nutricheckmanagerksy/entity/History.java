package com.ksy.nutricheckmanagerksy.entity;

import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import com.ksy.nutricheckmanagerksy.model.HistoryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class History {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "영양제 시퀀스")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pillId", nullable = false)
    private Pill pill;

    @ApiModelProperty(notes = "복용일자")
    @Column(nullable = false)
    private LocalDate eatDate;

    @ApiModelProperty(notes = "복용시간")
    @Column(nullable = false)
    private LocalTime eatTime;

    @ApiModelProperty(notes = "특이사항")
    @Column(length = 100)
    private String memo;

    private History(HistoryBuilder builder) {
        this.pill = builder.pill;
        this.eatDate = builder.eatDate;
        this.eatTime = builder.eatTime;
        this.memo = builder.memo;
    }

    public static class HistoryBuilder implements CommonModelBuilder<History> {
        private final Pill pill;
        private final LocalDate eatDate;
        private final LocalTime eatTime;
        private String memo;

        public HistoryBuilder(Pill pill, HistoryRequest request) {
            this.pill = pill;
            this.eatDate = request.getEatDate();
            this.eatTime = LocalTime.of(request.getEatTimeHour(), request.getEatTimeMinute());
        }

        public HistoryBuilder setMemo(String memo) {
            this.memo = memo;

            return this;
        }

        @Override
        public History build() {
            return new History(this);
        }
    }
}
