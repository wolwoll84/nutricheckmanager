package com.ksy.nutricheckmanagerksy.entity;

import com.ksy.nutricheckmanagerksy.enums.NutriInfo;
import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import com.ksy.nutricheckmanagerksy.model.PillInfoUpdateRequest;
import com.ksy.nutricheckmanagerksy.model.PillRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Pill {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "영양제")
    @Column(nullable = false, length = 35)
    private String pillName;

    @ApiModelProperty(notes = "제조사")
    @Column(nullable = false, length = 20)
    private String pillCompany;

    @ApiModelProperty(notes = "가격")
    @Column(nullable = false)
    private Integer pillPrice;

    @ApiModelProperty(notes = "영양정보")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private NutriInfo nutriInfo;

    @ApiModelProperty(notes = "복용시작일자 ((yyyy-mm-dd)")
    @Column(nullable = false)
    private LocalDate startDate;

    @ApiModelProperty(notes = "유통기한 (yyyy-mm-dd)")
    @Column(nullable = false)
    private LocalDate expirationDate;

    @ApiModelProperty(notes = "총 수량")
    @Column(nullable = false)
    private Integer totalQuantity;

    @ApiModelProperty(notes = "섭취주기")
    @Column(nullable = false)
    private Boolean isDaily; // "일간" : "주간"

    @ApiModelProperty(notes = "1회 복용량")
    @Column(nullable = false)
    private Integer oneTimeQuantity; // 1회 복용 개수

    @ApiModelProperty(notes = "재구매여부")
    @Column(nullable = false)
    private Integer remainingQuantity; // 잔여 수량

    @ApiModelProperty(notes = "복용여부")
    @Column(nullable = false)
    private Boolean isEat;

    public void putRemainingQuantity() {
        this.remainingQuantity = this.getRemainingQuantity() - this.oneTimeQuantity;
    }

    public void putPillInfo(PillInfoUpdateRequest request) {
        this.isDaily = request.getIsDaily();
        this.oneTimeQuantity = request.getOneTimeQuantity();
        this.remainingQuantity = request.getRemainingQuantity();
        this.isEat = request.getIsEat();
    }

    private Pill(PillBuilder builder) {
        this.pillName = builder.pillName;
        this.pillCompany = builder.pillCompany;
        this.pillPrice = builder.pillPrice;
        this.nutriInfo = builder.nutriInfo;
        this.startDate = builder.startDate;
        this.expirationDate = builder.expirationDate;
        this.totalQuantity = builder.totalQuantity;
        this.isDaily = builder.isDaily;
        this.oneTimeQuantity = builder.oneTimeQuantity;
        this.remainingQuantity = builder.remainingQuantity;
        this.isEat = builder.isEat;
    }

    public static class PillBuilder implements CommonModelBuilder<Pill> {
        private final String pillName;
        private final String pillCompany;
        private final Integer pillPrice;
        private final NutriInfo nutriInfo;
        private final LocalDate startDate;
        private final LocalDate expirationDate;
        private final Integer totalQuantity;
        private final Boolean isDaily;
        private final Integer oneTimeQuantity;
        private final Integer remainingQuantity;
        private final Boolean isEat;

        public PillBuilder(PillRequest request) {
            this.pillName = request.getPillName();
            this.pillCompany = request.getPillCompany();
            this.pillPrice = request.getPillPrice();
            this.nutriInfo = request.getNutriInfo();
            this.startDate = request.getStartDate();
            this.expirationDate = request.getExpirationDate();
            this.totalQuantity = request.getTotalQuantity();
            this.isDaily = request.getIsDaily();
            this.oneTimeQuantity = request.getOneTimeQuantity();
            this.remainingQuantity = this.totalQuantity;
            this.isEat = true;
        }

        @Override
        public Pill build() {
            return new Pill(this);
        }
    }
}
