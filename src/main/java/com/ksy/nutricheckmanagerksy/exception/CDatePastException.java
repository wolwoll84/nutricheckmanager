package com.ksy.nutricheckmanagerksy.exception;

public class CDatePastException extends RuntimeException {
    public CDatePastException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDatePastException(String msg) {
        super(msg);
    }

    public CDatePastException() {
        super();
    }
}
