package com.ksy.nutricheckmanagerksy.exception;

public class CExcessEatException extends RuntimeException {
    public CExcessEatException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExcessEatException(String msg) {
        super(msg);
    }

    public CExcessEatException() {
        super();
    }
}
