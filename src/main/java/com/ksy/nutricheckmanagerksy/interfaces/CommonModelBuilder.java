package com.ksy.nutricheckmanagerksy.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
