package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.entity.History;
import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryItem {
    @ApiModelProperty(notes = "복용내역 시퀀스")
    private Long historyId;

    @ApiModelProperty(notes = "영양제 제조사+이름")
    private String pillFullName;

    @ApiModelProperty(notes = "복용일자")
    private LocalDate eatDate;

    @ApiModelProperty(notes = "복용시간")
    private LocalTime eatTime;

    @ApiModelProperty(notes = "특이사항")
    private String memo;

    private HistoryItem(HistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.pillFullName = builder.pillFullName;
        this.eatDate = builder.eatDate;
        this.eatTime = builder.eatTime;
        this.memo = builder.memo;
    }

    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> {
        private final Long historyId;
        private final String pillFullName;
        private final LocalDate eatDate;
        private final LocalTime eatTime;
        private final String memo;

        public HistoryItemBuilder(History history) {
            this.historyId = history.getId();
            this.pillFullName = history.getPill().getPillCompany() + " / " + history.getPill().getPillName();
            this.eatDate = history.getEatDate();
            this.eatTime = history.getEatTime();
            this.memo = history.getMemo();
        }

        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
