package com.ksy.nutricheckmanagerksy.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class HistoryRequest {
    @ApiModelProperty(notes = "복용일자", required = true)
    @NotNull
    private LocalDate eatDate;

    @ApiModelProperty(notes = "복용시간/시 (0~23)", required = true)
    @NotNull
    @Min(0)
    @Max(23)
    private Integer eatTimeHour;

    @ApiModelProperty(notes = "복용시간/분 (0~59)", required = true)
    @NotNull
    @Min(0)
    @Max(59)
    private Integer eatTimeMinute;

    @ApiModelProperty(notes = "특이사항 (100자 이내)")
    @Length(max = 100)
    private String memo;
}
