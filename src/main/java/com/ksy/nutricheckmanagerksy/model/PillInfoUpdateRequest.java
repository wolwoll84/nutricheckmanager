package com.ksy.nutricheckmanagerksy.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PillInfoUpdateRequest {
    @ApiModelProperty(notes = "섭취주기", required = true)
    @NotNull
    private Boolean isDaily;

    @ApiModelProperty(notes = "1회 복용량 (1 이상)", required = true)
    @NotNull
    @Min(value = 1)
    private Integer oneTimeQuantity;

    @ApiModelProperty(notes = "잔여량", required = true)
    @NotNull
    @Min(value = 0)
    private Integer remainingQuantity;

    @ApiModelProperty(notes = "복용여부", required = true)
    @NotNull
    private Boolean isEat;
}
