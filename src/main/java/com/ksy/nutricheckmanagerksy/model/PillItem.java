package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.entity.Pill;
import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillItem {
    @ApiModelProperty(notes = "영양제 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "영양정보 이름")
    private String nutriInfoName;

    @ApiModelProperty(notes = "영양제 제조사+이름")
    private String pillFullName;

    @ApiModelProperty(notes = "1회 복용량")
    private Integer oneTimeQuantity;

    @ApiModelProperty(notes = "잔여량")
    private Integer remainingQuantity;

    @ApiModelProperty(notes = "복용여부")
    private String isEatName;

    private PillItem(SearchItemBuilder builder) {
        this.id = builder.id;
        this.nutriInfoName = builder.nutriInfoName;
        this.pillFullName = builder.pillFullName;
        this.oneTimeQuantity = builder.oneTimeQuantity;
        this.remainingQuantity = builder.remainingQuantity;
        this.isEatName = builder.isEatName;
    }

    public static class SearchItemBuilder implements CommonModelBuilder<PillItem> {
        private final Long id;
        private final String nutriInfoName;
        private final String pillFullName;
        private final Integer oneTimeQuantity;
        private final Integer remainingQuantity;
        private final String isEatName;

        public SearchItemBuilder(Pill pill) {
            this.id = pill.getId();
            this.nutriInfoName = pill.getNutriInfo().getName();
            this.pillFullName = pill.getPillCompany() + " / " + pill.getPillName();
            this.oneTimeQuantity = pill.getOneTimeQuantity();
            this.remainingQuantity = pill.getRemainingQuantity();
            this.isEatName = pill.getIsEat()? "O" : "X";
        }

        @Override
        public PillItem build() {
            return new PillItem(this);
        }
    }
}
