package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.entity.Pill;
import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillRepurchaseItem {
    private Long id;
    private String nutriInfoName;
    private String pillFullName;
    private Integer pillPrice;
    private Integer remainingQuantity;

    private PillRepurchaseItem(pillRepurchaseItemBuilder builder) {
        this.id = builder.id;
        this.nutriInfoName = builder.nutriInfoName;
        this.pillFullName = builder.pillFullName;
        this.pillPrice = builder.pillPrice;
        this.remainingQuantity = builder.remainingQuantity;
    }

    public static class pillRepurchaseItemBuilder implements CommonModelBuilder<PillRepurchaseItem> {
        @ApiModelProperty(notes = "영양제 시퀀스")
        private final Long id;

        @ApiModelProperty(notes = "영양정보 이름")
        private final String nutriInfoName;

        @ApiModelProperty(notes = "영양제")
        private final String pillFullName;

        @ApiModelProperty(notes = "가격")
        private final Integer pillPrice;

        @ApiModelProperty(notes = "잔여량")
        private final Integer remainingQuantity;

        public pillRepurchaseItemBuilder(Pill pill) {
            this.id = pill.getId();
            this.nutriInfoName = pill.getNutriInfo().getName();
            this.pillFullName = pill.getPillCompany() + " / " + pill.getPillName();
            this.pillPrice = pill.getPillPrice();
            this.remainingQuantity = pill.getRemainingQuantity();
        }

        @Override
        public PillRepurchaseItem build() {
            return new PillRepurchaseItem(this);
        }
    }
}
