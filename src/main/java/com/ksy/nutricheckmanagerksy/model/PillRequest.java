package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.enums.NutriInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PillRequest {
    @ApiModelProperty(notes = "영양제 (1~35자)", required = true)
    @NotNull
    @Length(min = 1, max = 35)
    private String pillName;

    @ApiModelProperty(notes = "제조사 (1~20자)", required = true)
    @NotNull
    @Length(min = 1, max = 20)
    private String pillCompany;

    @ApiModelProperty(notes = "가격 (0 이상)", required = true)
    @NotNull
    @Min(value = 0)
    private Integer pillPrice;

    @ApiModelProperty(notes = "영양정보", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private NutriInfo nutriInfo;

    @ApiModelProperty(notes = "복용시작일자 ((yyyy-mm-dd)", required = true)
    @NotNull
    private LocalDate startDate;

    @ApiModelProperty(notes = "유통기한 (yyyy-mm-dd)", required = true)
    @NotNull
    private LocalDate expirationDate;

    @ApiModelProperty(notes = "총 수량 (1 이상)", required = true)
    @NotNull
    @Min(value = 1)
    private Integer totalQuantity;

    @ApiModelProperty(notes = "섭취주기", required = true)
    @NotNull
    private Boolean isDaily;

    @ApiModelProperty(notes = "1회 복용량 (1 이상)", required = true)
    @NotNull
    @Min(value = 1)
    private Integer oneTimeQuantity;
}
