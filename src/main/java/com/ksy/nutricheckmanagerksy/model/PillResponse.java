package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.entity.Pill;
import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillResponse {
    @ApiModelProperty(notes = "영양제 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "제조사")
    private String pillCompany;

    @ApiModelProperty(notes = "영양제")
    private String pillName;

    @ApiModelProperty(notes = "가격")
    private String pillPriceName;

    @ApiModelProperty(notes = "영양정보")
    private String nutriInfoName;

    @ApiModelProperty(notes = "복용시작일자 (yyyy-mm-dd)")
    private LocalDate startDate;

    @ApiModelProperty(notes = "유통기한 (yyyy-mm-dd)")
    private LocalDate expirationDate;

    @ApiModelProperty(notes = "총 수량")
    private Integer totalQuantity;

    @ApiModelProperty(notes = "섭취주기")
    private Boolean isDaily; // 수정창으로 보낸다

    @ApiModelProperty(notes = "섭취주기 한글명")
    private String isDailyName; // 상세페이지로 보낸다

    @ApiModelProperty(notes = "1회 복용량")
    private Integer oneTimeQuantity;

    @ApiModelProperty(notes = "잔여량")
    private Integer remainingQuantity;

    @ApiModelProperty(notes = "복용여부")
    private Boolean isEat;

    @ApiModelProperty(notes = "복용여부 한글명")
    private String isEatName;

    private PillResponse(PillResponseBuilder builder) {
        this.id = builder.id;
        this.pillCompany = builder.pillCompany;
        this.pillName = builder.pillName;
        this.pillPriceName = builder.pillPriceName;
        this.nutriInfoName = builder.nutriInfoName;
        this.startDate = builder.startDate;
        this.expirationDate = builder.expirationDate;
        this.totalQuantity = builder.totalQuantity;
        this.isDaily = builder.isDaily;
        this.isDailyName = builder.isDailyName;
        this.oneTimeQuantity = builder.oneTimeQuantity;
        this.remainingQuantity = builder.remainingQuantity;
        this.isEat = builder.isEat;
        this.isEatName = builder.isEatName;
    }

    public static class PillResponseBuilder implements CommonModelBuilder<PillResponse> {
        private final Long id;
        private final String pillCompany;
        private final String pillName;
        private final String pillPriceName;
        private final String nutriInfoName;
        private final LocalDate startDate;
        private final LocalDate expirationDate;
        private final Integer totalQuantity;
        private final Boolean isDaily;
        private final String isDailyName;
        private final Integer oneTimeQuantity;
        private final Integer remainingQuantity;
        private final Boolean isEat;
        private final String isEatName;

        public PillResponseBuilder(Pill pill) {
            this.id = pill.getId();
            this.pillCompany = pill.getPillCompany();
            this.pillName = pill.getPillName();
            this.pillPriceName = pill.getPillPrice() + "원";
            this.nutriInfoName = pill.getNutriInfo().getName();
            this.startDate = pill.getStartDate();
            this.expirationDate = pill.getExpirationDate();
            this.totalQuantity = pill.getTotalQuantity();
            this.isDaily = pill.getIsDaily();
            this.isDailyName = pill.getIsDaily()? "일간" : "주간";
            this.oneTimeQuantity = pill.getOneTimeQuantity();
            this.remainingQuantity = pill.getRemainingQuantity();
            this.isEat = pill.getIsEat();
            this.isEatName = pill.getIsEat()? "복용중" : "복용하지 않음";
        }

        @Override
        public PillResponse build() {
            return new PillResponse(this);
        }
    }
}
