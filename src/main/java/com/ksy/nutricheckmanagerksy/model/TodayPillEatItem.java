package com.ksy.nutricheckmanagerksy.model;

import com.ksy.nutricheckmanagerksy.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TodayPillEatItem {
    @ApiModelProperty(notes = "영양제")
    private String pillFullName;

    @ApiModelProperty(notes = "등록 복용 횟수")
    private Integer dosesCount;

    @ApiModelProperty(notes = "실제 복용 횟수")
    private Integer realDosesCount;

    private TodayPillEatItem(TodayPillEatItemBuilder builder) {
        this.pillFullName = builder.pillFullName;
        this.dosesCount = builder.dosesCount;
        this.realDosesCount = builder.realDosesCount;
    }

    public static class TodayPillEatItemBuilder implements CommonModelBuilder<TodayPillEatItem> {
        private final String pillFullName;
        private final Integer dosesCount;
        private final Integer realDosesCount;

        public TodayPillEatItemBuilder(String pillFullName, Integer dosesCount, Integer realDosesCount) {
            this.pillFullName = pillFullName;
            this.dosesCount = dosesCount;
            this.realDosesCount = realDosesCount;
        }

        @Override
        public TodayPillEatItem build() {
            return new TodayPillEatItem(this);
        }
    }
}
