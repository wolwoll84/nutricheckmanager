package com.ksy.nutricheckmanagerksy.repository;

import com.ksy.nutricheckmanagerksy.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Long> {
    long countByPill_IdAndEatDate(long pillId, LocalDate eatDate);
    List<History> findAllByEatDate(LocalDate eatDate);
}
