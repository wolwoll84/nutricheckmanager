package com.ksy.nutricheckmanagerksy.repository;

import com.ksy.nutricheckmanagerksy.entity.Pill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PillRepository extends JpaRepository<Pill, Long> {
    List<Pill> findAllByIsDailyAndIsEatOrderByPillNameAsc(boolean isDaily, boolean isEat);
    List<Pill> findAllByPillNameLikeOrderByPillNameAsc(String searchName);
    List<Pill> findAllByRemainingQuantityLessThanEqualAndIsEatOrderByPillNameAsc(int quantity, boolean isEat);
}
