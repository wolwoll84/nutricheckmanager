package com.ksy.nutricheckmanagerksy.service;

import com.ksy.nutricheckmanagerksy.entity.History;
import com.ksy.nutricheckmanagerksy.model.HistoryItem;
import com.ksy.nutricheckmanagerksy.model.ListResult;
import com.ksy.nutricheckmanagerksy.repository.HistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final HistoryRepository historyRepository;

    public ListResult<HistoryItem> getHistories() {
        List<History> originList = historyRepository.findAll();

        List<HistoryItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HistoryItem.HistoryItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
