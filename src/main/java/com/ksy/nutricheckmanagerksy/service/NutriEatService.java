package com.ksy.nutricheckmanagerksy.service;

import com.ksy.nutricheckmanagerksy.entity.History;
import com.ksy.nutricheckmanagerksy.entity.Pill;
import com.ksy.nutricheckmanagerksy.exception.CEatDateFutureException;
import com.ksy.nutricheckmanagerksy.exception.CMissingDataException;
import com.ksy.nutricheckmanagerksy.model.HistoryRequest;
import com.ksy.nutricheckmanagerksy.model.ListResult;
import com.ksy.nutricheckmanagerksy.model.TodayPillEatItem;
import com.ksy.nutricheckmanagerksy.repository.HistoryRepository;
import com.ksy.nutricheckmanagerksy.repository.PillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NutriEatService {
    private final PillRepository pillRepository;
    private final HistoryRepository historyRepository;

    public void doEat(long pillId, HistoryRequest request) {
        Pill pill = pillRepository.findById(pillId).orElseThrow(CMissingDataException::new);

        LocalDate today = LocalDate.now();
//        long todayEat = historyRepository.countByPill_IdAndEatDate(pillId, request.getEatDate());

        if (today.isBefore(request.getEatDate())) throw new CEatDateFutureException();
//        if (pill.getOneTimeQuantity() <= todayEat) throw new CExcessEatException();
//        // 한번에 먹어야 하는 양과 오늘 먹은 횟수를 비교하니까 제대로 안 되는 거
//        // 대안. 엔티티에 하루에 먹어야 할 횟수를 추가한다

        History addData = new History.HistoryBuilder(pill, request).setMemo(request.getMemo()).build();
        historyRepository.save(addData);

        pill.putRemainingQuantity();
        pillRepository.save(pill);
    }

    public ListResult<TodayPillEatItem> getTodayStatus() {
        List<TodayPillEatItem> result = new LinkedList<>();

        List<Pill> pillList = pillRepository.findAllByIsDailyAndIsEatOrderByPillNameAsc(true, true);
        List<History> historyList = historyRepository.findAllByEatDate(LocalDate.now());

        for (Pill pillItem : pillList) {
            String pillFullName = pillItem.getPillCompany() + " " + pillItem.getPillName();
            int dosesCount = pillItem.getOneTimeQuantity();
            int realDosesCount = 0;

            for (History historyItem : historyList) {
                if (historyItem.getPill().getId().equals(pillItem.getId())) {
                    realDosesCount += 1;
                }
            }

            result.add(new TodayPillEatItem.TodayPillEatItemBuilder(pillFullName, dosesCount, realDosesCount).build());
        }

        return ListConvertService.settingResult(result);
    }
}
