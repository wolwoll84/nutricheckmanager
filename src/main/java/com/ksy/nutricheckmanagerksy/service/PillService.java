package com.ksy.nutricheckmanagerksy.service;

import com.ksy.nutricheckmanagerksy.entity.Pill;
import com.ksy.nutricheckmanagerksy.exception.CDatePastException;
import com.ksy.nutricheckmanagerksy.exception.CMissingDataException;
import com.ksy.nutricheckmanagerksy.model.*;
import com.ksy.nutricheckmanagerksy.repository.PillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PillService {
    private final PillRepository pillRepository;

    public void setPill(PillRequest request) {
        LocalDate today = LocalDate.now();

        if (today.isAfter(request.getExpirationDate())) throw new CDatePastException();
        Pill addData = new Pill.PillBuilder(request).build();

        pillRepository.save(addData);
    }

    public ListResult<PillItem> getPills() {
        List<Pill> originList = pillRepository.findAll();

        List<PillItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new PillItem.SearchItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<PillItem> getPillsByLikeName(String searchName) {
        List<Pill> originList = pillRepository.findAllByPillNameLikeOrderByPillNameAsc("%" + searchName + "%");

        List<PillItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new PillItem.SearchItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<PillRepurchaseItem> getRepurchases() {
        List<Pill> originList = pillRepository.findAllByRemainingQuantityLessThanEqualAndIsEatOrderByPillNameAsc(15, true);

        List<PillRepurchaseItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new PillRepurchaseItem.pillRepurchaseItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public PillResponse getPillDetail(long id) {
        Pill originData = pillRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new PillResponse.PillResponseBuilder(originData).build();
    }

    public void putPillInfo(long id, PillInfoUpdateRequest request) {
        Pill originData = pillRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putPillInfo(request);

        pillRepository.save(originData);
    }
}
